// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");
const fs = require("fs")
const ethers = hre.ethers

let addresses = require('../addresses/deployment.json');

async function main() {
  switch (hre.network.name) {
    case 'kovan':
      providerAddress = addresses.kovan.LendingAddressesProvider
      protocolDataProvider = addresses.kovan.ProtocolDataProvider
      principalTokenAddress = '0xFf795577d9AC8bD7D90Ee22b6C1703490b6512FD'
      break;
    case 'mainnet':
      providerAddress = addresses.mainnet.LendingAddressesProvider
      protocolDataProvider = addresses.mainnet.ProtocolDataProvider
      principalTokenAddress = '0x6b175474e89094c44da98b954eedeac495271d0f'
      break;
  }
  console.log('Deploying Collateral Token...')
  const CollateralToken = await ethers.getContractFactory("CollateralToken");
  const collateralToken = await CollateralToken.deploy()
  await collateralToken.approve(collateralToken.address, ethers.constants.MaxUint256)
  console.log("CollateralToken deployed to:", collateralToken.address);
  console.log('Deploying Collaterals...');
  const Collaterals = await ethers.getContractFactory("Collaterals");
  const collaterals = await Collaterals.deploy(collateralToken.address)
  await collateralToken.transfer(collaterals.address, ethers.utils.parseEther('10000'))
  console.log("Collaterals deployed to:", collaterals.address);
  console.log('Deploying TrustMinimizedBond...');
  const TrustMinimizedBond = await ethers.getContractFactory("TrustMinimizedBond");
  const trustMinimizedBond = await TrustMinimizedBond.deploy(principalTokenAddress, providerAddress, protocolDataProvider, collateralToken.address)
  console.log("trustMinimizedBond deployed to:", trustMinimizedBond.address);
  addresses[hre.network.name]['ETMB'] = trustMinimizedBond.address
  addresses[hre.network.name]['ECT'] = collateralToken.address
  addresses[hre.network.name]['Collateral'] = collaterals.address
  fs.writeFileSync('../addresses/deployment.json', JSON.stringify(addresses, null, 2))
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
