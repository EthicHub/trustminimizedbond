require("@nomiclabs/hardhat-waffle");
require('@openzeppelin/hardhat-upgrades');
require("@nomiclabs/hardhat-etherscan");

let secrets = require('./.secrets.json')
const IERC20 = require('./external-abis/IERC20.json')

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

task("approveERC20", "Approves an ERC20 token")
.addParam("tokenAddress", "The token to approve")
.addParam("spender", "The address that will pu")
.setAction(async ({ tokenAddress, spender }, hre) => {
  const ethers = hre.ethers
  const [owner, signer] = await hre.ethers.getSigners();

  const erc20 = new ethers.Contract(tokenAddress, IERC20, signer);
  const tx = await erc20.approve(spender, ethers.constants.MaxUint256 )
  console.log(tx)
})

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.6.12",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.7.6",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.8.2",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      }
    ],
  },
  networks: {
    local: {
      url: 'http://127.0.0.1:8545',
    },
    kovan: {
      url: secrets.kovan.node_url,
      chainId: 42,
      accounts: secrets.kovan.pks,
      gasPrice: 1000000000, // 1Gwei
    },
    mainnet: {
      url: secrets.mainnet.node_url,
      chainId: 1,
      accounts: secrets.mainnet.pks,
    },
  },
  etherscan: {
    apiKey: secrets.etherscan_api_key
  }
};
