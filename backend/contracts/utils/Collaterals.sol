// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract Collaterals {
    using SafeERC20 for IERC20;
    mapping(uint256 => uint256) public collaterals;
    uint256 public emmitedCollateralCounter;
    IERC20 public token;

    constructor(address tokenAddress)  {
        require(tokenAddress != address(0), "Collaterals::Invalid token address");

        token = IERC20(tokenAddress);
        emmitedCollateralCounter = 0;
    }

    function _calculateCollateralAssigned(uint256 amount, uint256 collateralPrice) internal returns(uint256) {
        uint256 multiplier = 2;
        uint256 collateralAssigned = collateralPrice * amount* multiplier;

        return collateralAssigned;
    }

    function _addEmmitedCollateralCounter(uint256 collateralAssigned) internal {
        emmitedCollateralCounter = emmitedCollateralCounter + collateralAssigned;
    }

    function _subEmmitedCollateralCounter(uint256 tokenId, uint256 debt, uint256 collateralPrice) internal {
        uint256 collateralAssigned = collaterals[tokenId]; 
        uint256 collateralDebt = debt * collateralPrice;
        if (collateralDebt >= collateralAssigned) {
            emmitedCollateralCounter = emmitedCollateralCounter - collateralAssigned;
        } else {
            emmitedCollateralCounter = emmitedCollateralCounter - collateralDebt;
        }

        collaterals[tokenId] = 0;
    }

    function addCollateral(uint256 tokenId, uint256 amount, uint256 collateralPrice) public {
        uint256 collateralAssigned = _calculateCollateralAssigned(amount, collateralPrice);
        uint256 balance = token.balanceOf(address(this));

        require((collateralAssigned + emmitedCollateralCounter) <= balance, "Collaterals:: Insufficient funds");

        collaterals[tokenId] = collateralAssigned;

        _addEmmitedCollateralCounter(collateralAssigned);
    }

    function _calculateCollateralLiquidation(uint256 tokenId, uint256 defaultAmount, uint256 collateralPrice) internal returns(uint256) {
        uint256 collateralizedAmount = collaterals[tokenId]; // defaultAmount es la cantidad que no se ha pagado.
        uint256 collateralLiquidation = collateralizedAmount * collateralPrice;

        return collateralLiquidation;
    }

    function transferCollateral(address _to, uint256 amount) internal returns(bool) { 
        token.safeTransfer(_to, amount);

        return true;
    } 
}
