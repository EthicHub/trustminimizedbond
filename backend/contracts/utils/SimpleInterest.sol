// SPDX-License-Identifier: gpl-3.0
pragma solidity 0.8.2;


contract SimpleInterest {

  uint256 immutable ONE = 10^18;
  uint256 immutable SECONDS_IN_YEAR = 31556952;

  function simpleInterest(uint256 principal, uint256 interest, uint256 elapsedTime) internal view returns (uint256) {
    return principal*ONE+interest*elapsedTime/SECONDS_IN_YEAR/100;
  }
}
