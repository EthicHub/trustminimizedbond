// SPDX-License-Identifier: gpl-3.0
pragma solidity 0.8.2;

import "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";

contract CollateralToken is ERC20PresetMinterPauser {
    constructor() public ERC20PresetMinterPauser("Ethix Collateral Token", "ECT")  {
        _mint(msg.sender, 10000000000000*10**18);
    }

    function mint(uint256 amount) external {
      _mint(msg.sender, amount);
    }
}