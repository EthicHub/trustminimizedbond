// SPDX-License-Identifier: gpl-3.0
pragma solidity 0.8.2;

import './interfaces/IAddressesProvider.sol';
import './interfaces/ILendingPool.sol';
import './interfaces/IProtocolDataProvider.sol';

abstract contract AAVECreditDelegator {
    uint16 internal constant REFERRAL_CODE = 1;
    IAddressesProvider public addressesProvider;
    IProtocolDataProvider public protocolDataProvider;
    ILendingPool public lendingPool;

    event DelegatedCredit(address indexed asset, address indexed delegator, uint256 amount, uint256 interestRateMode);

    modifier correctInterestRateMode(uint256 interestRateMode) {
        require(interestRateMode == 1 || interestRateMode == 2, "Wrong interest rate mode");
        _;
    }

    constructor(address _addressesProvider, address _protocolDataProvider){
        addressesProvider = IAddressesProvider(_addressesProvider);
        protocolDataProvider = IProtocolDataProvider(_protocolDataProvider);
        lendingPool = ILendingPool(addressesProvider.getLendingPool());

    }

    function getReserveTokensAddresses(address assetToBorrow)
      external view returns (address aTokenAddress, address stableDebtTokenAddress, address variableDebtTokenAddress) {
      return protocolDataProvider.getReserveTokensAddresses(assetToBorrow);
    }
    

    function _borrow(uint256 amount, uint256 interestRateMode, address assetToBorrow)
      correctInterestRateMode(interestRateMode)
      internal {
        lendingPool.borrow(assetToBorrow, amount, interestRateMode, REFERRAL_CODE, msg.sender);
        emit DelegatedCredit(assetToBorrow, msg.sender, amount, interestRateMode);
    }   

    
}