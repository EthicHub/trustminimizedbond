// SPDX-License-Identifier: gpl-3.0
pragma solidity 0.8.2;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./utils/Collaterals.sol";
import "./utils/SimpleInterest.sol";
import "./AAVECreditDelegator.sol";
import "./utils/Collaterals.sol";

contract TrustMinimizedBond is
  ERC721,
  ERC721Enumerable,
  ERC721URIStorage, 
  ERC721Burnable,
  SimpleInterest,
  AAVECreditDelegator,
  Collaterals
  {
    using Counters for Counters.Counter;

    Counters.Counter private _tokenIdCounter;
    // TODO Get collateralPrice from Oracle.
    uint256 internal constant COLLATERAL_PRICE = 7000000000000000000;
    
    struct Bond {
        uint256 mintingDate;
        uint256 maturity;
        uint256 principal;
        uint256 interest; //annual
    }

    mapping(uint256 => Bond) public bonds;
    IERC20 public principalToken;

    event BondCreated(uint256 id, uint256 mintingDate, uint256 maturity, uint256 principal, uint256 interest);
    event BondRedeemed(uint256 id, uint256 redeemDate, uint256 maturity, uint256 withdrawn, uint256 interest);

    constructor(address _principalTokenAddress,address _providerAddress, address _protocolDataProvider, address _collateralTokenAddress)
      ERC721 ("Ethic TMB", "ETMB")
      AAVECreditDelegator(_providerAddress, _protocolDataProvider)
      Collaterals(_collateralTokenAddress)
       {
        principalToken = IERC20(_principalTokenAddress);
    }

    function safeMint(address to) internal {
        _safeMint(to, _tokenIdCounter.current());
        _tokenIdCounter.increment();
    }
    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }
    
    function _burn(uint256 tokenId) internal override (ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function _createBond(uint256 id, uint256 maturity, uint256 principal, uint256 interest) internal {
      require(interest > 0, "TrustMinimizedBond::Interest cant be 0");
      require(principal > 0, "TrustMinimizedBond::Principal cant be 0");
      bonds[id] = Bond(block.timestamp, maturity, principal, interest);
      emit BondCreated(id, block.timestamp, maturity, principal, interest);
    }

    function buyBond(string calldata tokenURI, uint256 maturity, uint256 principal, uint256 interest)
        external
        returns (uint256)
    {   

        require(principalToken.transferFrom(msg.sender, address(this), principal));

        return _buyBond(tokenURI, maturity, principal, interest);
    }

    function buyBondAAVE(uint256 interestRateMode, string calldata tokenURI, uint256 maturity, uint256 principal, uint256 interest) external returns (uint256) {
        _borrow(principal, interestRateMode, address(principalToken));
        return _buyBond(tokenURI, maturity, principal, interest);
    }

    function _buyBond(string calldata tokenURI, uint256 maturity, uint256 principal, uint256 interest) internal returns (uint256) {
        uint256 newItemId = _tokenIdCounter.current();
        _mint(msg.sender, newItemId);
        _setTokenURI(newItemId, tokenURI);
        _createBond(newItemId, maturity, principal, interest);
        _tokenIdCounter.increment();
        addCollateral(newItemId, principal, COLLATERAL_PRICE);
        return newItemId;
    }

    function redeemBond(uint256 tokenId) external {
      require(canRedeem(tokenId), "TrustMinimizedBond: Can't redeem yet");
      _burn(tokenId);
      uint256 withdrawAmount = positionValue(tokenId);
      uint256 partialWithdrawAmount = withdrawAmount - address(this).balance;
      Bond memory bond = bonds[tokenId];
      delete bonds[tokenId];
      if (withdrawAmount <= token.balanceOf(address(this))) {
          require(principalToken.transfer(msg.sender, withdrawAmount));
      } else {
          require(principalToken.transfer(msg.sender, partialWithdrawAmount));
          require(transferCollateral(msg.sender, (withdrawAmount - partialWithdrawAmount)));
      }
      _subEmmitedCollateralCounter(tokenId, (withdrawAmount - partialWithdrawAmount), COLLATERAL_PRICE);

      emit BondRedeemed(tokenId, block.timestamp, bond.maturity, withdrawAmount, bond.interest);
    }

    function positionValue(uint256 tokenId) public view returns(uint256) {
      Bond memory bond = bonds[tokenId];
      if (block.timestamp >= bond.maturity + bond.mintingDate) {
        return simpleInterest(bond.principal, bond.interest, bond.maturity);
      }
      return simpleInterest(bond.principal, bond.interest, block.timestamp - bond.mintingDate);
    }

    function canRedeem(uint256 tokenId) public view returns(bool) {
      Bond memory bond = bonds[tokenId];
      return block.timestamp >= bond.maturity + bond.mintingDate;
    }

}
