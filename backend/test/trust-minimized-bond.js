/*
    Trust Minimized Bond test.

    Copyright (C) 2021 EthicHub

    This file is part of EthicHub.

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { ethers } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");
const { parseEther, formatEther } = require('@ethersproject/units')
const { BigNumber } = require('@ethersproject/bignumber')

use(solidity);

describe("Trust Minimized Bond", function () {
  let bond, principalToken, owner, principal

  describe("TrustMinimizedBond", function () {
    it("Should deploy TrustMinimizedBond", async function () {
      const signers = await ethers.getSigners();
      owner = signers[0];
      const PrincipalToken = await ethers.getContractFactory("PrincipalToken");
      principalToken = await PrincipalToken.deploy();
      const TrustMinimizedBond = await ethers.getContractFactory("TrustMinimizedBond");
      bond = await TrustMinimizedBond.deploy();
      await bond.initialize(principalToken.address, "TrustMinimizedBond", "TMB");

      const tokenAdd = await bond.principalToken()
      expect(principalToken.address).to.equal(tokenAdd)
    });

    describe("buyBond", function () {
      it("Should be able to buy bond", async function () {
        const initialBalance = await principalToken.balanceOf(owner.address)
        console.log('initialBalance', formatEther(initialBalance))
        principal = parseEther('10')
        const tx = await principalToken.approve(bond.address, parseEther('111111111111111111111111111111111111'))

        await bond.buyBond('http://localhost:3000', 600, principal, 10)
        const finalBalance = await principalToken.balanceOf(owner.address)
        console.log('finalBalance', formatEther(finalBalance))

        expect(finalBalance).to.equal(initialBalance.sub(principal))
        const bondBalance = await bond.balanceOf(owner.address)
        console.log(bondBalance)
        expect(bondBalance).to.equal(BigNumber.from('1'))
        const mintedBond = await bond.bonds(BigNumber.from('1'))
        expect(mintedBond['interest']).to.equal(BigNumber.from(10))

      });
      it("Should increase interest", async function() {
        await ethers.provider.send("evm_increaseTime", [10000])
        const value = await bond.positionValue(BigNumber.from('1'))
        expect(value).to.gt(principal)

      })
    });
  });
});