export default {
  ADD_TX(state, { tag, status, txHash}) {
    state.txs[tag] = { status, txHash }
    state.txs = {...state.txs}
  },
  UPDATE_STATUS(state, { tag, status }) {
    state.txs[tag] = status
    state.txs = {...state.txs}

  }

}
