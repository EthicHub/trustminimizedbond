
export default {
  async executeTx({ commit }, { action, tag }) {
    console.log(action)
    commit('ADD_TX', { tag, txHash: null, status: 'PENDING' })
    let tx
    try {
      tx = await action
      const receipt = await tx.wait()
      commit('ADD_TX', { tag, txHash: tx.hash, status: 'SUCCESS' })
      return receipt
    } catch (err) {
      commit('UPDATE_STATUS', { tag, status: 'ERROR' })

      console.log(err)
    }
  },
}
