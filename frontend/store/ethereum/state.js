export default () => ({
  ethersBalance: null,
  isEthersReady: false,
  currentAddress: null,
  isWalletConnected: false
});
