import { BigNumber } from 'ethers'

export default {
  async getEthersBalance({ commit, state }) {
    const provider = this.$provider.provider
    console.log(provider)
    if (!provider) return
    const balance = state.currentAddress
      ? await provider.getBalance(state.currentAddress)
      : BigNumber.from('0')
    commit('setEthersBalance', balance)
  },
}
