export default {
  setEthersProvider(state, provider) {
    state.ethersProvider = provider
  },
  setEthersBalance(state, balance) {
    state.ethersBalance = balance
  },
  setIsEthersReady(state, ready) {
    state.isEthersReady = ready
  },
  setCurrentAddress(state, address) {
    state.currentAddress = address
  },
  setIsWalletConnected(state, connected) {
    state.isWalletConnected = connected
  }
}
