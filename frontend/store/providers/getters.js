export default {
  connectJsonRpcLoading: (state) => state.connectJsonRpcLoading,
  connectMetamaskLoading: (state) => state.connectMetamaskLoading,
  connectWalletConnectLoading: (state) => state.connectWalletConnectLoading,
}
