export default {
  CONNECT_JSON_RPC_PROVIDER_REQUEST(state) {
    state.connectJsonRpcLoading = true
  },

  CONNECT_JSON_RPC_PROVIDER_SUCCESS(state) {
    state.connectJsonRpcLoading = false
  },

  CONNECT_JSON_RPC_PROVIDER_FAILURE(state) {
    state.connectJsonRpcLoading = false
  },
  CONNECT_METAMASK_REQUEST(state) {
    state.connectMetamaskLoading = true
  },

  CONNECT_METAMASK_SUCCESS(state) {
    state.connectMetamaskLoading = false
  },

  CONNECT_METAMASK_FAILURE(state) {
    state.connectMetamaskLoading = false
  },

  CONNECT_WALLET_CONNECT_REQUEST(state) {
    state.connectWalletConnectLoading = true
  },

  CONNECT_WALLET_CONNECT_SUCCESS(state) {
    state.connectWalletConnectLoading = false
  },

  CONNECT_WALLET_CONNECT_FAILURE(state) {
    state.connectWalletConnectLoading = false
  },
}
