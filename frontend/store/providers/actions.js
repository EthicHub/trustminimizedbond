import { ethers } from "ethers";

export default {
  connectJsonRpc({ commit }) {
    commit("CONNECT_JSON_RPC_PROVIDER_REQUEST");

    try {
      commit("CONNECT_JSON_RPC_PROVIDER_SUCCESS");

      const isJsonRpcProvider = true;

      const jsonRpcProvider = new ethers.providers.JsonRpcProvider(
        `https://${process.env.VUE_APP_NETWORK}.infura.io/v3/${process.env.VUE_APP_INFURA_ID}`
      );

      this.$provider.setProvider(jsonRpcProvider, isJsonRpcProvider);

      localStorage.removeItem("provider");

      localStorage.setItem("provider", process.env.VUE_APP_JSON_RPC_PROVIDER);

      return Promise.resolve();
    } catch (error) {
      commit("CONNECT_JSON_RPC_PROVIDER_FAILURE");

      return Promise.reject(error);
    }
  },

  async connectMetamask({ commit }) {
    commit("CONNECT_METAMASK_REQUEST");

    try {
      const accounts = await window.ethereum.request({
        method: "eth_requestAccounts"
      });

      if (accounts.length !== 0) {
        const provider = window.ethereum;

        const web3Provider = new ethers.providers.Web3Provider(provider);

        this.$provider.setProvider(web3Provider);
      }

      localStorage.removeItem("provider");

      localStorage.setItem("provider", "metamask");

      commit("ethereum/setIsEthersReady", true, { root: true });

      commit("CONNECT_METAMASK_SUCCESS");

      return Promise.resolve();
    } catch (error) {
      commit("CONNECT_METAMASK_FAILURE");

      return Promise.reject(error);
    }
  },

  async connectWalletConnect({ commit }) {
    commit("CONNECT_WALLET_CONNECT_REQUEST");

    try {
      const provider = this.$walletConnectProvider;

      await provider.enable();

      const web3Provider = new ethers.providers.Web3Provider(provider);

      this.$provider.setProvider(web3Provider);

      localStorage.removeItem("provider");

      commit("ethereum/setIsEthersReady", true, { root: true });

      commit("CONNECT_WALLET_CONNECT_SUCCESS");

      return Promise.resolve();
    } catch (error) {
      commit("CONNECT_WALLET_CONNECT_FAILURE");

      return Promise.reject(error);
    }
  }
};
