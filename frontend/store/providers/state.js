export default () => ({
  connectJsonRpcLoading: false,
  connectMetamaskLoading: false,
  connectWalletConnectLoading: false,
})
