import Web3Modal from 'web3modal'

class Web3ModalPlugin {
  web3Modal = null
  provider = null

  initWeb3Modal() {
    const providerOptions = {}

    const web3Modal = new Web3Modal({
      network: kovan,
      cacheProvider: true,
      providerOptions,
      disableInjectedProvider: false,
    })

    this.web3Modal = web3Modal
  }

  async clearCachedProvider() {
    await this.web3Modal.clearCachedProvider()
  }

  async initProvider() {
    try {
      this.provider = await this.web3Modal.connect()
    } catch (error) {
      console.log(error)
    }
  }
}

const web3Modal = new Web3ModalPlugin()

export default (_, inject) => {
  inject('web3Modal', web3Modal)
}