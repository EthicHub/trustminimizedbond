class Provider {
  provider = null
  cachedProvider = null
  signer = null

  setProvider(provider, isJsonRpcProvider) {
    this.provider = provider

    if (!isJsonRpcProvider) {
      this.signer = provider.getSigner()
    }
  }
}

const provider = new Provider()

export default (_, inject) => {
  inject('provider', provider)
}
