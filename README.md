
# MinimiMice

Buy Trust Minimized fixed yield bonds delegating your AAVE credit and earn extra yield on your borrow capacity

# Slides

[Link](https://docs.google.com/presentation/d/1-Kv-fDt3oHz7eYKQP7ZS6lLN7ZDiiP1D3Sk148PcFxQ/edit?usp=sharing)

# Kovan Address:
[0xe48781014ef13246b3344a3a8fba07963e5a790d](https://kovan.etherscan.io/address/0xe48781014ef13246b3344a3a8fba07963e5a790d)

# Steps

Go to https://staging.aave.com/
Connect Metamask to Kovan
Mint tokens with faucet
Deposit all the moneys to get borrowing capacity (demo is hardcoded to delegate 1000 DAI)

```
cd frontend
npm i
npm run dev
```

Open http://localhost:3000


Follow steps!